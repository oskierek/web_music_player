<<?php 
	
	if(isset($_POST["loginButton"])){
		// login button pressed
		$username = $_POST['username'];
	}

	if(isset($_POST["registerButton"])){
		// register button pressed
	}

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>My Web Music Player</title>
 </head>
 <body>
 	<div id="inputContainer">
 		<form id="loginForm" action="register.php" method="POST">
 			<h2>Login to your account:</h2>
 			<p>
 				<label for="loginUsername">Username</label>
 				<input type="text" name="loginUsername" id="loginUsername" placeholder="Username" required>
 			</p>
 			<p>
 				<label for="loginPassword">Password</label>
 				<input type="text" name="loginPassword" id="loginPassword" placeholder="password" required>
 			</p>

 			<button type="submit" name="loginButton">Log in!</button>
 		</form>
 		
 		<form id="registerForm" action="register.php" method="POST">
 			<h2>Create your account:</h2>
 			<p>
 				<label for="username">Username</label>
 				<input type="text" name="username" id="username" required>
 			</p>

 			<p>
 				<label for="firstName">First name</label>
 				<input type="text" name="firstName" id="firstName" required>
 			</p>

 			<p>
 				<label for="lastName">Last name</label>
 				<input type="text" name="lastName" id="lastName" required>
 			</p>

 			<p>
 				<label for="email">Email</label>
 				<input type="email" name="email" id="email" required>
 			</p>

 			<p>
 				<label for="password">Enter your password:</label>
 				<input type="password" name="password" id="password" required>
 			</p>
			<p>
 				<label for="password1">Repeate your password:</label>
 				<input type="password" name="password1" id="password1" required>
 			</p>

 		</form>
 	</div>
 </body>
 </html>